import Image from 'next/image';
import Link from 'next/link';
import React from 'react';
import devImg from '../public/assets/dev.jpg'

const About = () => {
    return (
        <div id='about' className='w-full md:h-screen p-2 flex items-center py-16'>
            <div className='max-w-[1240px] m-auto md:grid grid-cols-3 gap-8'>
                <div className='col-span-2'>
                    <p className='uppercase text-xl tracking-widest text-[#5651e5]'>A propos</p>
                    <h2 className='py-4'>Qui je suis</h2>
                    <p className='py-2 text-gray-600'><em>&quot; Il faut que la volonté imagine trop pour réaliser assez &quot;</em></p>
                    <p className='py-2 text-gray-600'>
                        Passionné par la réalisation et le développement de projets web, je me suis tout naturellement tourné vers une carrière de développeur web après une
                        reconversion professionnelle. Formé par OpenClassRooms, je dispose de plusieurs compétences complémentaires pour réaliser vos projets.
                    </p>
                    <p className='py-2 text-gray-600'>
                        Après 5 ans de ventes automobiles, je me suis demandé ce que je voulais vraiment faire et ce qui pouvait me rendre heureux.
                        Apprendre comment un site fonctionne, le recréé et voir le résultat est quelque chose de très satisfaisant. Savoir comment fonctionne
                        X ou Y fonctionnalité, animation et savoir les reproduire ! Voilà ce qui me motive, réaliser le site qu&apos;imagine le client.
                    </p>
                    <Link href="/#projects">
                        <p className='py-2 text-gray-600 underline cursor-pointer'>Jetez un oeil a mes projets.</p>
                    </Link>
                </div>
                <div className='w-full h-auto m-auto shadow-xl shadow-gray-400 rounded-xl flex items-center justify-center p-4 hover:scale-105 ease-in duration-300'>
                    <Image className='rounded-xl' src={devImg} alt='dev web logo' width='1000' height='1000' />
                </div>
            </div>
        </div>
    );
};

export default About;