import Link from 'next/link';
import React from 'react';

const Demo = ({videoUrl}) => {
    return (
        <div className='w-screen h-screen flex flex-col py-10 items-center justify-center'>
           <iframe src={videoUrl} width="1100px" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
           <Link href='/hottakes'>
                <p className='underline cursor-pointer uppercase font-bold'>Retour</p>
            </Link>
        </div>
    );
};

export default Demo;