import { useRouter } from 'next/router';
import React from 'react';
import { AiOutlineMail } from 'react-icons/ai';
import { FaGitlab, FaLinkedinIn } from 'react-icons/fa';
import { HiOutlineDocumentText } from 'react-icons/hi';

const Main = () => {
    const router = useRouter()

    return (
        <div className='w-full h-screen text-center'>
            <div className='max-w-[1240px] w-full h-full mx-auto p-2 flex justify-center items-center'>
                <div>
                    <p className='uppercase text-sm tracking-widest text-gray-600'>Il faut que la volonté imagine trop pour réaliser assez.</p>
                    <h1 className='py-4 text-gray-700'>
                        Bonjour, je suis <span className='text-[#5651e5]'>Kévin</span>
                    </h1>
                    <h1 className='py-2 text-gray-700'>
                        Un Développeur Web Front-End
                    </h1>
                    <p className='py-4 text-gray-600 max-w-[70%] m-auto'>
                    Dynamique et motivé, je me passionne pour le développement du côté Front-End en utilisant principalement React et NextJs. 
                    Je suis curieux des nouvelles technologies et continue d&apos;améliorer mes compétences tout au long de mes projets.
                    </p>
                    <div className='flex items-center justify-between max-w-[330px] m-auto py-4'>
                        <div onClick={() => open('https://www.linkedin.com/in/kévintoudic')} className='rounded-full shadow-lg shadow-gray-400 p-6 cursor-pointer hover:scale-105 ease-in duration-300'>
                            <FaLinkedinIn />
                        </div>
                        <div onClick={() => open('https://gitlab.com/kevintdc')} className='rounded-full shadow-lg shadow-gray-400 p-6 cursor-pointer hover:scale-105 ease-in duration-300'>
                            <FaGitlab />
                        </div>
                        <div onClick={() => router.push('mailto:toudickevin@gmail.com')} className='rounded-full shadow-lg shadow-gray-400 p-6 cursor-pointer hover:scale-105 ease-in duration-300'>
                            <AiOutlineMail />
                        </div>
                        <div onClick={() => open('https://acrobat.adobe.com/id/urn:aaid:sc:EU:b89eadd1-86d6-4f7e-8a8a-896c188791a8')} className='rounded-full shadow-lg shadow-gray-400 p-6 cursor-pointer hover:scale-105 ease-in duration-300'>
                            <HiOutlineDocumentText />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Main;