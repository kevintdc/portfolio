import React from 'react';
import bookiImg from '../public/assets/projects/booki.png'
import ohmyfoodImg from '../public/assets/projects/ohmyfood.png'
import lapanthereImg from '../public/assets/projects/lapanthere.png'
import hottakesImg from '../public/assets/projects/hottakes.png'
import kanapImg from '../public/assets/projects/kanap.png'
import kasaImg from '../public/assets/projects/kasa.png'
import ProjectItem from './ProjectItem';

const Projects = () => {
    return (
        <div id='projects' className='w-full'>
            <div className='max-w-[1240px] mx-auto px-2 py-16'>
                <p className='text-xl tracking-widest uppercase text-[#5156e5]'>Mes Projets</p>
                <h2 className='py-4'>Ce que j&apos;ai fait</h2>
                <div className='grid md:grid-cols-2 gap-8'>

                <ProjectItem title='Booki' backgroundImg={bookiImg} projectUrl='/booki' description='Intégration maquette HTML/CSS' />
                <ProjectItem title='OhMyFood' backgroundImg={ohmyfoodImg} projectUrl='/ohmyfood' description='Animation avancée avec SASS' />
                <ProjectItem title='Agence La Panthere' backgroundImg={lapanthereImg} projectUrl='/lapanthere' description='Optimisez un site web existant' />
                <ProjectItem title='Kanap' backgroundImg={kanapImg} projectUrl='/kanap' description='Site e-commerce en JavaScript' />
                <ProjectItem title='Hot Takes' backgroundImg={hottakesImg} projectUrl='/hottakes' description='Création d&apos;une API sécurisée' />
                <ProjectItem title='Kasa' backgroundImg={kasaImg} projectUrl='/kasa' description='Création d&apos;une application web avec React'/>

                </div>
            </div>
        </div>
    );
};

export default Projects;