import Image from 'next/image';
import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { AiOutlineMail } from 'react-icons/ai';
import { FaGitlab, FaLinkedinIn } from 'react-icons/fa';
import { HiOutlineChevronDoubleUp, HiOutlineDocumentText } from 'react-icons/hi'
import contact from '../public/assets/contact.jpg'

const Contact = () => {
    const router = useRouter()

    return (
        <div id='contact' className='w-full lg:h-screen'>
            <div className='max-w-[1240px] m-auto px-2 py-16 w-full'>
                <p className='text-xl tracking-widest uppercase text-[#5651e5]'>Contact</p>
                <h2 className='py-4'>Comment me joindre</h2>
                <div className='grid lg:grid-cols-5 gap-8'>
{/* left */}
                <div className='col-span-3 lg:col-span-2 w-full h-full shadow-xl shadow-gray-400 rounded-xl p-4'>
                    <div className='lg:p-4 h-full'>
                        <div>
                            <Image className='rounded-xl hover:scale-105 ease-in duration-30' src={contact} alt="/" />
                        </div>
                        <div className='pt-10'>
                            <h2 className='py-2'>Kévin Toudic</h2>
                            <p>Développeur Front-End</p>
                            <p className='py-4'>Vous souhaitez des informations supplémentaires ? Qu&apos;on travaille sur un projet ensemble ?</p>
                        </div>
                    
                    </div>
 
                </div>
{/* right */}
                <div className='w-[850px]'>
                            <p className='uppercase pt-8 text-center'>Connectons nous</p>
                        <div className='flex-col items-center justify-between py-8 w-full'>
                        <div onClick={() => open('https://www.linkedin.com/in/kévintoudic')} className='rounded-full flex justify-center shadow-lg shadow-gray-400 p-6 cursor-pointer hover:scale-105 ease-in duration-300'>
                            <FaLinkedinIn />
                        </div>
                        <div onClick={() => open('https://gitlab.com/kevintdc')} className='rounded-full flex justify-center shadow-lg shadow-gray-400 p-6 cursor-pointer hover:scale-105 ease-in duration-300'>
                            <FaGitlab />
                        </div>
                        <div onClick={() => router.push('mailto:toudickevin@gmail.com')} className='rounded-full flex justify-center shadow-lg shadow-gray-400 p-6 cursor-pointer hover:scale-105 ease-in duration-300'>
                            <AiOutlineMail />
                        </div>
                        <div onClick={() => open('https://acrobat.adobe.com/id/urn:aaid:sc:EU:b89eadd1-86d6-4f7e-8a8a-896c188791a8')} className='rounded-full flex justify-center shadow-lg shadow-gray-400 p-6 cursor-pointer hover:scale-105 ease-in duration-300'>
                            <HiOutlineDocumentText />
                        </div>
                        </div>
                    </div>
                {/* <div className='col-span-3 w-full h-auto shadow-xl shadow-gray-400 rounded-xl lg:p-4'>
                    <div className='p-4'>
                        <form>
                            <div className='grid md:grid-cols-2 gap-4 w-ful py-2'>
                            <div className='flex flex-col'>
                                <label className='uppercase text-sm py-2'>Nom Prénom *</label>
                                <input className='border-2 rounded-lg p-3 flex border-gray-300' type="text" required placeholder='Pierre Dupont' minLength='3' maxLength='30'/>
                            </div>
                            <div className='flex flex-col'>
                                <label className='uppercase text-sm py-2'>Téléphone</label>
                                <input className='border-2 rounded-lg p-3 flex border-gray-300' type="tel" required placeholder='06 12 34 56 78'/>
                            </div>
                            </div>
                            <div className='flex flex-col py-2'>
                                <label className='uppercase text-sm py-2'>E-mail *</label>
                                <input className='border-2 rounded-lg p-3 flex border-gray-300' type="email" required placeholder='contact@email.com'/>
                            </div>
                            <div className='flex flex-col py-2'>
                                <label className='uppercase text-sm py-2'>Objet *</label>
                                <input className='border-2 rounded-lg p-3 flex border-gray-300' type="text" required minLength='6'/>
                            </div>
                            <div className='flex flex-col py-2'>
                                <label className='uppercase text-sm py-2'>Message *</label>
                                <textarea className='border-2 rounded-lg p-3 border-gray-300' rows='10' required minLength='10'></textarea>
                            </div>
                            <button className='p-4 text-gray-100 mt-4 w-full'>Envoyer le message</button>
                        </form>
                    </div>
                </div> */}
                </div>
                <div className='flex justify-center py-12'>
                    <Link href='/'>
                        <div className='rounded-full shadow-lg shadow-gray-400 p-4 cursor-pointer hover:scale-105 ease-in duration-300'>
                            <HiOutlineChevronDoubleUp className='text-[#5651e5]' size={30}/>
                        </div>
                    </Link>
                </div>
            </div>
        </div>
    );
};

export default Contact;