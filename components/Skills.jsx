import Image from 'next/image';
import React from 'react';
import htmlImg from '../public/assets/skills/html.png'
import cssImg from '../public/assets/skills/css.png'
import javascriptImg from '../public/assets/skills/javascript.png'
import nodeImg from '../public/assets/skills/node.png'
import reactImg from '../public/assets/skills/react.png'
import nextjsImg from '../public/assets/skills/nextjs.png'
import tailwindImg from '../public/assets/skills/tailwind.png'
import mongoImg from '../public/assets/skills/mongo.png'
import gitlabImg from '../public/assets/skills/gitlab.png'

const Skills = () => {
    return (
        <div id='skills' className='w-full lg:h-screen p-2'>
            <div className='max-w-[1240px] mx-auto flex flex-col justify-center h-full'>
                <p className='text-xl tracking-widest uppercase text-[#5156e5]'>Compétences</p>
                <h2 className='py-4'>Ce que je peux faire</h2>
                <div className='grid md:grid-cols-2 lg:grid-cols-4 gap-8'>

                    <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
                    <div className='grid grid-cols-2 gap-4 justify-center items-center'>
                        <div className='m-auto'>
                            <Image src={htmlImg} alt='html logo' width='64' height='64'/>
                        </div>
                        <div className='flex flex-col items-center justify-center'>
                            <h3>HTML</h3>
                        </div>
                    </div>
                    </div>

                    <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
                    <div className='grid grid-cols-2 gap-4 justify-center items-center'>
                        <div className='m-auto'>
                            <Image src={cssImg} alt='css logo' width='64' height='64'/>
                        </div>
                        <div className='flex flex-col items-center justify-center'>
                            <h3>CSS</h3>
                        </div>
                    </div>
                    </div>
                    
                    <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
                    <div className='grid grid-cols-2 gap-4 justify-center items-center'>
                        <div className='m-auto'>
                            <Image src={javascriptImg} alt='javascript logo' width='64' height='64'/>
                        </div>
                        <div className='flex flex-col items-center justify-center'>
                            <h3>JAVASCRIPT</h3>
                        </div>
                    </div>
                    </div>
                    
                    <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
                    <div className='grid grid-cols-2 gap-4 justify-center items-center'>
                        <div className='m-auto'>
                            <Image src={nodeImg} alt='node js logo' width='64' height='64'/>
                        </div>
                        <div className='flex flex-col items-center justify-center'>
                            <h3>NODE</h3>
                        </div>
                    </div>
                    </div>
                    
                    <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
                    <div className='grid grid-cols-2 gap-4 justify-center items-center'>
                        <div className='m-auto'>
                            <Image src={reactImg} alt='react logo' width='64' height='64'/>
                        </div>
                        <div className='flex flex-col items-center justify-center'>
                            <h3>REACT</h3>
                        </div>
                    </div>
                    </div>
                    
                    <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
                    <div className='grid grid-cols-2 gap-4 justify-center items-center'>
                        <div className='m-auto'>
                            <Image src={nextjsImg} alt='next js logo' width='64' height='64'/>
                        </div>
                        <div className='flex flex-col items-center justify-center'>
                            <h3>NEXT JS</h3>
                        </div>
                    </div>
                    </div>
                    
                    <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
                    <div className='grid grid-cols-2 gap-4 justify-center items-center'>
                        <div className='m-auto'>
                            <Image src={tailwindImg} alt='tailwind logo' width='64' height='64'/>
                        </div>
                        <div className='flex flex-col items-center justify-center'>
                            <h3>TAILWIND</h3>
                        </div>
                    </div>
                    </div>
                    
                    <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
                    <div className='grid grid-cols-2 gap-4 justify-center items-center'>
                        <div className='m-auto'>
                            <Image src={mongoImg} alt='mongo db logo' width='64' height='64'/>
                        </div>
                        <div className='flex flex-col items-center justify-center'>
                            <h3>MONGO DB</h3>
                        </div>
                    </div>
                    </div>
                    
                    {/* <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
                    <div className='grid grid-cols-2 gap-4 justify-center items-center'>
                        <div className='m-auto'>
                            <Image src="/../public/assets/skills/firebase.png" alt='html logo' width='64' height='64'/>
                        </div>
                        <div className='flex flex-col items-center justify-center'>
                            <h3>FIREBASE</h3>
                        </div>
                    </div>
                    </div> */}
                    
                    <div className='p-6 shadow-xl rounded-xl hover:scale-105 ease-in duration-300'>
                    <div className='grid grid-cols-2 gap-4 justify-center items-center'>
                        <div className='m-auto'>
                            <Image src={gitlabImg} alt='gitlab logo' width='64' height='64'/>
                        </div>
                        <div className='flex flex-col items-center justify-center'>
                            <h3>GITLAB</h3>
                        </div>
                    </div>
                    </div>
                    
                </div>
            </div>
        </div>
    );
};

export default Skills;