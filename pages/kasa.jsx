import Image from 'next/image'
import Link from 'next/link'
import React from 'react'
import { RiRadioButtonFill } from 'react-icons/ri'
import kasaImg from '../public/assets/projects/kasa.png'

const kasa = () => {
  return (
    <div className='w-full'>
        <div className='w-screen h-[30vh] lg:h-[40vh] relative'>
            <div className='absolute top-0 left-0 w-full h-[30vh] lg:h-[40vh] bg-black/80 z-10' />
            <Image className='absolute z-1' layout="fill" objectFit='cover' src={kasaImg} alt='/' />
            <div className='absolute top-[70%] max-w-[1240px] w-full left-[50%] right-[50%] translate-x-[-50%] translate-y-[-50%] z-10 text-white p-2'>
                <h2 className='py-2'>Kasa</h2>
                <h3>Création d&apos;une application web avec React</h3>
            </div>
        </div>

        <div className='max-w-[1240px] mx-auto p-2 grid md:grid-cols-5 gap-8 pt-8'>
            <div className='col-span-4'>
                <p>Projet</p>
                <h2 className='py-3'>Contexte</h2>
                <p>Kasa vous recrute en tant que développeur front-end en freelance pour développer sa nouvelle plateforme web. 
                    Kasa est dans le métier de la location d&apos;appartements entre particuliers depuis près de 10 ans maintenant. 
                    Avec plus de 500 annonces postées chaque jour, Kasa fait partie des leaders de la location d&apos;appartements entre particuliers en France.
                    L&apos;occasion parfaite pour vous d&apos;ajouter une belle référence à votre portfolio de freelance !
                    Le site de Kasa a été codé il y a maintenant plus de 10 ans en ASP.NET avec un code legacy important. 
                    Laura, la CTO, a donc lancé une refonte totale pour passer à une stack complète en JavaScript avec NodeJS côté back-end, et React côté front-end. 
                    Kasa en a également profité pour commander de nouvelles maquettes auprès de son designer habituel, qui est en freelance. Un gros chantier pour cette année !
                </p>
                <button onClick={() => open('https://p7-kasa-beige.vercel.app/')} className='px-8 py-2 mt-4 mr-8'>Demo</button>
                <button onClick={() => open('https://gitlab.com/openclassrooms_dw/p7_kasa')} className='px-8 py-2 mt-4'>Code</button>
            </div>
            <div className='col-span-4 md:col-span-1 shadow-xl shadow-gray-400 rounded-xl p-4'>
                <div className='p-2'>
                    <p className='text-center font-bold pb-2'>Technologies</p>
                    <div className='grid grid-cols-3 md:grid-cols-1'>
                        <p className='flex items-center text-gray-600 py-2'><RiRadioButtonFill className='pr-1'/>JAVASCRIPT</p>
                        <p className='flex items-center text-gray-600 py-2'><RiRadioButtonFill className='pr-1'/>REACT</p>
                        <p className='flex items-center text-gray-600 py-2'><RiRadioButtonFill className='pr-1'/>NODE JS</p>
                    </div>
                </div>
            </div>
            <Link href='/#projects'>
                <p className='underline cursor-pointer uppercase font-bold'>Retour</p>
            </Link>
        </div>
    </div>
  )
}

export default kasa