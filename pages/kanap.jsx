import Image from 'next/image'
import Link from 'next/link'
import React from 'react'
import { RiRadioButtonFill } from 'react-icons/ri'
import kanapImg from '../public/assets/projects/kanap.png'

const kanap = () => {
  return (
    <div className='w-full'>
        <div className='w-screen h-[30vh] lg:h-[40vh] relative'>
            <div className='absolute top-0 left-0 w-full h-[30vh] lg:h-[40vh] bg-black/80 z-10' />
            <Image className='absolute z-1' layout="fill" objectFit='cover' src={kanapImg} alt='/' />
            <div className='absolute top-[70%] max-w-[1240px] w-full left-[50%] right-[50%] translate-x-[-50%] translate-y-[-50%] z-10 text-white p-2'>
                <h2 className='py-2'>Kanap</h2>
                <h3>Site e-commerce en JavaScript</h3>
            </div>
        </div>

        <div className='max-w-[1240px] mx-auto p-2 grid md:grid-cols-5 gap-8 pt-8'>
            <div className='col-span-4'>
                <p>Projet</p>
                <h2 className='py-3'>Contexte</h2>
                <p>Votre client est Kanap, une marque de canapés qui vend ses produits depuis sa boutique exclusivement. 
                    Aujourd&apos;hui, celle-ci souhaiterait avoir une plateforme de e-commerce en plus de sa boutique physique pour vendre ses produits sur Internet.
                    La partie front-end et l&apos;API existe déjà. Je dois réaliser l&apos;intégration des éléments de façon dynamique et la liaison a l&apos;API.
                </p>
                <button onClick={() => open('/demo/dem_kanap')} className='px-8 py-2 mt-4 mr-8'>Demo</button>
                <button onClick={() => open('https://gitlab.com/openclassrooms_dw/P5')} className='px-8 py-2 mt-4'>Code</button>
            </div>
            <div className='col-span-4 md:col-span-1 shadow-xl shadow-gray-400 rounded-xl p-4'>
                <div className='p-2'>
                    <p className='text-center font-bold pb-2'>Technologies</p>
                    <div className='grid grid-cols-3 md:grid-cols-1'>
                        <p className='flex items-center text-gray-600 py-2'><RiRadioButtonFill className='pr-1'/>JAVASCRIPT</p>
                        <p className='flex items-center text-gray-600 py-2'><RiRadioButtonFill className='pr-1'/>NODE JS</p>
                    </div>
                </div>
            </div>
            <Link href='/#projects'>
                <p className='underline cursor-pointer uppercase font-bold'>Retour</p>
            </Link>
        </div>
    </div>
  )
}

export default kanap