import Image from 'next/image'
import Link from 'next/link'
import React from 'react'
import { RiRadioButtonFill } from 'react-icons/ri'
import ohmyfoodImg from '../public/assets/projects/ohmyfood.png'

const ohmyfood = () => {
  return (
    <div className='w-full'>
        <div className='w-screen h-[30vh] lg:h-[40vh] relative'>
            <div className='absolute top-0 left-0 w-full h-[30vh] lg:h-[40vh] bg-black/80 z-10' />
            <Image className='absolute z-1' layout="fill" objectFit='cover' src={ohmyfoodImg} alt='/' />
            <div className='absolute top-[70%] max-w-[1240px] w-full left-[50%] right-[50%] translate-x-[-50%] translate-y-[-50%] z-10 text-white p-2'>
                <h2 className='py-2'>OhMyFood</h2>
                <h3>Animation avancée avec SASS</h3>
            </div>
        </div>

        <div className='max-w-[1240px] mx-auto p-2 grid md:grid-cols-5 gap-8 pt-8'>
            <div className='col-span-4'>
                <p>Projet</p>
                <h2 className='py-3'>Contexte</h2>
                <p>Ohmyfood! est une jeune startup qui voudrait s&apos;imposer sur le marché de la restauration. 
                    L&apos;objectif est de développer un site 100% mobile qui répertorie les menus de restaurants gastronomiques. 
                    En plus des systèmes classiques de réservation, les clients pourront composer le menu de leur repas pour que les plats soient prêts à leur arrivée. 
                    Finis, les temps d&apos;attente au restaurant !
                </p>
                <button onClick={() => open('https://openclassrooms_dw.gitlab.io/P3/')} className='px-8 py-2 mt-4 mr-8'>Demo</button>
                <button onClick={() => open('https://gitlab.com/openclassrooms_dw/P3')} className='px-8 py-2 mt-4'>Code</button>
            </div>
            <div className='col-span-4 md:col-span-1 shadow-xl shadow-gray-400 rounded-xl p-4'>
                <div className='p-2'>
                    <p className='text-center font-bold pb-2'>Technologies</p>
                    <div className='grid grid-cols-3 md:grid-cols-1'>
                        <p className='flex items-center text-gray-600 py-2'><RiRadioButtonFill className='pr-1'/>HTML</p>
                        <p className='flex items-center text-gray-600 py-2'><RiRadioButtonFill className='pr-1'/>CSS</p>
                        <p className='flex items-center text-gray-600 py-2'><RiRadioButtonFill className='pr-1'/>SASS</p>
                    </div>
                </div>
            </div>
            <Link href='/#projects'>
                <p className='underline cursor-pointer uppercase font-bold'>Retour</p>
            </Link>
        </div>
    </div>
  )
}

export default ohmyfood