import Image from 'next/image'
import Link from 'next/link'
import React from 'react'
import { RiRadioButtonFill } from 'react-icons/ri'
import lapanthereImg from '../public/assets/projects/lapanthere.png'

const lapanthere = () => {
  return (
    <div className='w-full'>
        <div className='w-screen h-[30vh] lg:h-[40vh] relative'>
            <div className='absolute top-0 left-0 w-full h-[30vh] lg:h-[40vh] bg-black/80 z-10' />
            <Image className='absolute z-1' layout="fill" objectFit='cover' src={lapanthereImg} alt='/' />
            <div className='absolute top-[70%] max-w-[1240px] w-full left-[50%] right-[50%] translate-x-[-50%] translate-y-[-50%] z-10 text-white p-2'>
                <h2 className='py-2'>Agence La Panthère</h2>
                <h3>Optimiser le référencement d&apos;un site existant</h3>
            </div>
        </div>

        <div className='max-w-[1240px] mx-auto p-2 grid md:grid-cols-5 gap-8 pt-8'>
            <div className='col-span-4'>
                <p>Projet</p>
                <h2 className='py-3'>Contexte</h2>
                <p>La Panthère, une grande agence de web design basée à Lyon. L&apos;activité de l&apos;entreprise a bien démarré mais aujourd&apos;hui, elle est en perte de vitesse.
                La fondatrice de l&apos;entreprise cherche une solution pour faire repartir l&apos;activité. 
                En tapant “Entreprise web design Lyon” sur Internet, elle s&apos;aperçoit que le site de l&apos;agence apparaît seulement en deuxième page des moteurs de recherche.
                Je dois réaliser un audit du site et l&apos;améliorer afin d&apos;optimiser son référencement.
                </p>
                <button onClick={() => open('https://openclassrooms_dw.gitlab.io/p4/')} className='px-8 py-2 mt-4 mr-8'>Demo</button>
                <button onClick={() => open('https://gitlab.com/openclassrooms_dw/p4')} className='px-8 py-2 mt-4'>Code</button>
            </div>
            <div className='col-span-4 md:col-span-1 shadow-xl shadow-gray-400 rounded-xl p-4'>
                <div className='p-2'>
                    <p className='text-center font-bold pb-2'>Technologies</p>
                    <div className='grid grid-cols-3 md:grid-cols-1'>
                        <p className='flex items-center text-gray-600 py-2'><RiRadioButtonFill className='pr-1'/>HTML</p>
                        <p className='flex items-center text-gray-600 py-2'><RiRadioButtonFill className='pr-1'/>CSS</p>
                        <p className='flex items-center text-gray-600 py-2'><RiRadioButtonFill className='pr-1'/>NODE JS</p>
                    </div>
                </div>
            </div>
            <Link href='/#projects'>
                <p className='underline cursor-pointer uppercase font-bold'>Retour</p>
            </Link>
        </div>
    </div>
  )
}

export default lapanthere