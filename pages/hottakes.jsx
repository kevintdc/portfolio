import Image from 'next/image'
import Link from 'next/link'
import React from 'react'
import { RiRadioButtonFill } from 'react-icons/ri'
import hottakesImg from '../public/assets/projects/hottakes.png'

const hottakes = () => {
  return (
    <div className='w-full'>
        <div className='w-screen h-[30vh] lg:h-[40vh] relative'>
            <div className='absolute top-0 left-0 w-full h-[30vh] lg:h-[40vh] bg-black/80 z-10' />
            <Image className='absolute z-1' layout="fill" objectFit='cover' src={hottakesImg} alt='/' />
            <div className='absolute top-[70%] max-w-[1240px] w-full left-[50%] right-[50%] translate-x-[-50%] translate-y-[-50%] z-10 text-white p-2'>
                <h2 className='py-2'>Hot Takes</h2>
                <h3>Création d&apos;une API sécurisée</h3>
            </div>
        </div>

        <div className='max-w-[1240px] mx-auto p-2 grid md:grid-cols-5 gap-8 pt-8'>
            <div className='col-span-4'>
                <p>Projet</p>
                <h2 className='py-3'>Contexte</h2>
                <p>La semaine dernière, vous avez reçu un message sur votre plateforme de freelance vous demandant de l&apos;aide pour un nouveau projet. 
                    Les sauces piquantes sont de plus en plus populaires, en grande partie grâce à la série YouTube « Hot Ones » . 
                    C&apos;est pourquoi ce nouveau client, la marque de condiments à base de piment Piiquante, veut développer une application web de critique des sauces piquantes appelée « Hot Takes ».
                     Si la responsable produit de Piiquante souhaite à terme transformer l&apos;application d&apos;évaluation en une boutique en ligne, elle souhaite que la première version soit une « galerie de sauces » 
                     permettant aux utilisateurs de télécharger leurs sauces piquantes préférées et de liker ou disliker les sauces que d&apos;autres partagent. 
                    Le front-end de l&apos;application a été développé à l&apos;aide d&apos;Angular et a été précompilé après des tests internes, mais Piiquante a besoin d&apos;un développeur back-end pour construire l&apos;API.
                </p>
                <button onClick={() => open('/demo/dem_hottakes')} className='px-8 py-2 mt-4 mr-8'>Demo</button>
                <button onClick={() => open('https://gitlab.com/openclassrooms_dw/p6')} className='px-8 py-2 mt-4'>Code</button>
            </div>
            <div className='col-span-4 md:col-span-1 shadow-xl shadow-gray-400 rounded-xl p-4'>
                <div className='p-2'>
                    <p className='text-center font-bold pb-2'>Technologies</p>
                    <div className='grid grid-cols-3 md:grid-cols-1'>
                        <p className='flex items-center text-gray-600 py-2'><RiRadioButtonFill className='pr-1'/>JAVASCRIPT</p>
                        <p className='flex items-center text-gray-600 py-2'><RiRadioButtonFill className='pr-1'/>NODE JS</p>
                        <p className='flex items-center text-gray-600 py-2'><RiRadioButtonFill className='pr-1'/>MONGO DB</p>
                    </div>
                </div>
            </div>
            <Link href='/#projects'>
                <p className='underline cursor-pointer uppercase font-bold'>Retour</p>
            </Link>
        </div>
    </div>
  )
}

export default hottakes